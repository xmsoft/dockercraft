var io=function(uri,cb){
    var s={
        events:{},
        on:function(e,cb){
            s.events[e]=cb;
        },
        onmessage:function(msg){
            msg=JSON.parse(msg.data);
            if(s.events[msg.e]!=null){
                s.events[msg.e](msg.data);
            }
        },
        onclose:function(){
            if(s.events["close"]!=null){
                s.events["close"]();
            }
        },
        emit:function(e,data){
            socket.send(JSON.stringify({e:e,data:data}));
        }
    }
    var socket=new WebSocket(uri);
    socket.onmessage=s.onmessage;
    socket.onclose=s.onclose;
    socket.onopen=cb;
    return s;
}