var socket;
$(document).ready(function(){
   if(socket==null){
    socket = io("ws://127.0.0.1:8088/ws/",function(){

            socket.on("instances/list",function(data){
                    data=JSON.parse(data);

                    $(".main").html(jQuery.tmpl($("#tpl_instlist").val(), data));

            })
            socket.on("touch_terminal",function(data){
                 data=JSON.parse(data);
                 $(".main").html(jQuery.tmpl($("#tpl_terminal").val(),{uuid:data.uuid,url:"terminal.html?server="+escape(data.url)}));
                                 //http://127.0.0.1:8088/terminal.html?server=ws%3a%2f%2f127.0.0.1%3a3001%2fws%3ftime%3d1503475656%26sign%3dabcdefg
            });
            socket.on("settings",function(data){
                //data=JSON.parse(data);

            });
        });
   }


   $(".rc").click(function(){
        socket.emit($(this).attr("href"),"");
        return false;
   });
   $(".terminal-cog").click(function(){
    alert("ok");
   })
});
var touch=function(uuid){
    socket.emit("touch_terminal",uuid);
}
var settings=function(uuid){
    $(".main").html(jQuery.tmpl($("#tpl_settings").val(),{src:"/instance/settings/"+uuid}));
}
var inst = {
     start:function(uuid){
        socket.emit("inst.start",uuid);
     },
     stop:function(uuid){
        if(confirm("此操作将强制停止此容器，所有未保存的数据将会丢失！是否继续？")){
            socket.emit("inst.stop",uuid);
        }

     },
     restart:function(uuid){
        if(confirm("此操作将强制重启此容器，所有未保存的数据将会丢失！是否继续？")){
            socket.emit("inst.restart",uuid);
        }
     }
}