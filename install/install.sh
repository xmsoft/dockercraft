#!/bin/bash
killall docker nginx
apt-get -y autoremove docker* nginx
mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak
apt-get update
apt-get install -y apt-transport-https ca-certificates curl software-properties-common nginx
curl -fsSL https://download.daocloud.io/docker/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=$(dpkg --print-architecture)] https://download.daocloud.io/docker/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install -y -q docker-ce=17.03.1*
curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://2480f742.m.daocloud.io
service docker restart
chmod 7777 /var/run/docker.sock
curl -R server_ngx_conf > /etc/nginx/nginx.conf
service nginx restart
iptables -P INPUT ACCEPT
echo "安装完成"