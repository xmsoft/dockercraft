package cn.tohsaka.factory.dockercraft.Libraries

import java.lang.System.out

/**
 * Created by Xmsoft on 2017/8/4.
 */
class Logger(val clazz:Class<*>){
    private fun print(l:Int,obj:Any){
        var level = arrayOf("[warn]","[debug]","[trace]","[info]");
        print(level[l-1]);
        println(obj);
    }
    fun warn(obj:Any){
        print(0,obj);
    }
    fun debug(obj:Any){
        print(1,obj);
    }
    fun trace(obj:Any){
        print(2,obj);
    }
    fun info(obj:Any){
        print(3,obj);
    }
    fun printCopyright(){
        println("Copyright Dockercraft,Designed by Xmsoft All rights reserved.");
        println(" ________      ________      ________      ___  __        _______       ________     \n" +
                "|\\   ___ \\    |\\   __  \\    |\\   ____\\    |\\  \\|\\  \\     |\\  ___ \\     |\\   __  \\    \n" +
                "\\ \\  \\_|\\ \\   \\ \\  \\|\\  \\   \\ \\  \\___|    \\ \\  \\/  /|_   \\ \\   __/|    \\ \\  \\|\\  \\   \n" +
                " \\ \\  \\ \\\\ \\   \\ \\  \\\\\\  \\   \\ \\  \\        \\ \\   ___  \\   \\ \\  \\_|/__   \\ \\   _  _\\  \n" +
                "  \\ \\  \\_\\\\ \\   \\ \\  \\\\\\  \\   \\ \\  \\____    \\ \\  \\\\ \\  \\   \\ \\  \\_|\\ \\   \\ \\  \\\\ \\/ \n" +
                "   \\ \\_______\\   \\ \\_______\\   \\ \\_______\\   \\ \\__\\\\ \\__\\   \\ \\_______\\   \\ \\__\\\\ _\\ \n" +
                "    \\|_______|    \\|_______|    \\|_______|    \\|__| \\|__|    \\|_______|    \\|__|\\|__|\n" +
                "           ________      ________      ________      ________  _________                       \n" +
                "          |\\   ____\\    |\\   __  \\    |\\   __  \\    |\\  _____\\|\\___   ___\\                     \n" +
                "          \\ \\  \\___|    \\ \\  \\|\\  \\   \\ \\  \\|\\  \\   \\ \\  \\__/ \\|___ \\  \\_|                     \n" +
                "           \\ \\  \\        \\ \\   _  _\\   \\ \\   __  \\   \\ \\   __\\     \\ \\  \\                      \n" +
                "            \\ \\  \\____    \\ \\  \\\\  \\|   \\ \\  \\ \\  \\   \\ \\  \\_|      \\ \\  \\                     \n" +
                "             \\ \\_______\\   \\ \\__\\\\ _\\    \\ \\__\\ \\__\\   \\ \\__\\        \\ \\__\\                    \n" +
                "              \\|_______|    \\|__|\\|__|    \\|__|\\|__|    \\|__|         \\|__|         \n");

    }
}