package cn.tohsaka.factory.dockercraft.Libraries;

import cn.tohsaka.factory.dockercraft.Interfaces.EventBasedWebSocketConnection;
import org.webbitserver.WebSocketConnection;

import java.util.List;

/**
 * Created by Xmsoft on 2017/8/20.
 */
public abstract class EventHandler {
    public abstract void onEvent(String e,Object data,EventBasedWebSocketConnection wsc);
    public static void emit(List<EventHandler> events, String e, Object data, EventBasedWebSocketConnection wsc){
        for (EventHandler eh:events) {
            eh.onEvent(e,data,wsc);
        }
    }
}
