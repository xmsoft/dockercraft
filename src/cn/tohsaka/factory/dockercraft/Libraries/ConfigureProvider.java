package cn.tohsaka.factory.dockercraft.Libraries;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by Xmsoft on 2016/11/9.
 */
public class ConfigureProvider {
    public static Properties prop = new Properties();
    public static void load(){
        try {
            prop.load(new FileInputStream(new File(System.getProperty("user.dir") + File.separator + "config.properties")));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static Object get(Object name){
        return prop.get(name);
    }
    public static String get(String name){
        return prop.getProperty(name);
    }
}