package cn.tohsaka.factory.dockercraft.Webutils;

import cn.tohsaka.factory.dockercraft.Structs.Users;
import cn.tohsaka.factory.dockercraft.Utils;
import cn.tohsaka.factory.dockercraft.kvjsonBuilder;
import cn.tohsaka.factory.dockercraft.utils.HttpHandlerEx;
import cn.tohsaka.factory.dockercraft.utils.session;
import cn.tohsaka.factory.dockercraft.utils.sessions;
import org.webbitserver.HttpControl;
import org.webbitserver.HttpHandler;
import org.webbitserver.HttpRequest;
import org.webbitserver.HttpResponse;

import java.net.HttpCookie;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Xmsoft on 2017/8/17.
 */
public class wUser extends HttpHandlerEx {

    @Override
    public void onRequest(HttpRequest req, session session, HttpResponse res, HttpControl rec) {
        if(req.uri().contains("Login")){
            Users user=Users.verifyUser(req.postParam("un"),req.postParam("pwd"));
            if(user==null){
                res.content(new kvjsonBuilder().put("result",false).getJson()).end();
            }
            session.set("user",user);
            session.set("uid",(int)user.uid);
            session.set("un",user.un);
            res.content(new kvjsonBuilder().put("result",true).getJson()).end();
        }
    }
}
