package cn.tohsaka.factory.dockercraft.utils

import java.io.File
import java.io.FileWriter
import java.util.*

/**
 * Created by Xmsoft on 2016/8/11.
 */
class diskutils {
    fun getFileContent(file:File):String{
        return file.readText(Charsets.UTF_8);
    }
    fun copyDir(dir: File, targetDir: File) {
        var list = scandir(dir);
        for (file in list) {
            file.copyTo(File(targetDir.absolutePath + File.separator + file.relativeTo(dir).toString()), true);
        }
    }

    fun copyFile(file: File, targetDir: File) {
        var targetFile = File(targetDir.absolutePath + File.separator + file.name);
        if (!targetFile.parentFile.exists()) {
            targetDir.mkdirs();
        }
        file.copyTo(targetFile, true);
    }

    fun scandir(dir: File): ArrayList<File> {
        var files = ArrayList<File>();
        for (file in dir.listFiles()) {
            if (file.isDirectory()) {
                files.addAll(scandir(file));
            } else {
                files.add(file);
            }
        }
        return files;
    }
}
