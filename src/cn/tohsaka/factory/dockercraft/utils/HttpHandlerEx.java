package cn.tohsaka.factory.dockercraft.utils;

import org.webbitserver.*;
import org.webbitserver.HttpRequest;

/**
 * Created by Xmsoft on 2017/8/20.
 */
public abstract class HttpHandlerEx implements HttpHandler {
    @Override
    public void handleHttpRequest(HttpRequest httpRequest, HttpResponse httpResponse, HttpControl httpControl) throws Exception {
        onRequest(httpRequest,sessions.get(httpRequest.cookieValue("sessid")),httpResponse,httpControl);
    }
    public abstract void onRequest(HttpRequest req,session session,HttpResponse res,HttpControl rec);
}
