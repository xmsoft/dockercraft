package cn.tohsaka.factory.dockercraft.Engines;

import cn.tohsaka.factory.dockerclient.DockerClient;
import cn.tohsaka.factory.dockercraft.Structs.Node;
import cn.tohsaka.factory.eoh.interfaces.DBRequester;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Xmsoft on 2017/8/14.
 */
public class Nodes {
    private static List<Node> dockerClients;
    private static Map<String,DockerClient> provider=new HashMap<>();
    private static Map<String,Node> nodes=new HashMap<>();
    public static DockerClient get(String id){
        return provider.get(id);
    }
    public static DockerClient getByLabel(String label){
        for (Node d:dockerClients) {
            if(d.label.equalsIgnoreCase(label)){
                return provider.get(d.uuid);
            }
        }
        return null;
    }
    public static Map<String,DockerClient> getProvider(){
        return provider;
    }
    public static Collection<DockerClient> getList(){
        return provider.values();
    }
    public static void init() {
        dockerClients=new DBRequester<Node>(Node.class).getAll("SELECT * FROM "+new Node().getTable());
        for (Node d:dockerClients) {
            provider.put(d.uuid,new DockerClient(d.ip).setRemoteKey(d.auth));
            nodes.put(d.uuid,d);
        }
    }
    public static Node getNode(String uuid){
        return nodes.get(uuid);
    }
}
