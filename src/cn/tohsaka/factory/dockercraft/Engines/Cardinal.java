package cn.tohsaka.factory.dockercraft.Engines;
import cn.tohsaka.factory.dockerclient.beans.DockerResponse;
import cn.tohsaka.factory.dockerclient.beans.details.create.CreateContainerCmd;
import cn.tohsaka.factory.dockerclient.beans.details.create.CreateContainerResponse;
import cn.tohsaka.factory.dockerclient.beans.details.create.PortBinding;
import cn.tohsaka.factory.dockerclient.beans.details.create.RestartPolicy;
import cn.tohsaka.factory.dockercraft.Structs.Instance;

/**
 * Created by Xmsoft on 2017/8/3.
 */
public class Cardinal {
    public static void init() {
    }
    public static String CreateInstance(Instance instance) throws Exception{
        CreateContainerCmd ccc=new CreateContainerCmd();
        ccc.withImage("q1051278389/dcterminal:latest")

                .withDictionaryBind(String.format("/mnt/data/idata/%s",instance.uuid.toLowerCase()),"/data","rw")
                .withDictionaryBind("/mnt/data/servers","/servers","ro")
                .withPortBinding("25565",new PortBinding("0.0.0.0","25565"))
                .withRestartPolicy(RestartPolicy.always())
                .withExposedPort("25565")
                .withMemory(Long.valueOf(instance.mem))
                .withCpus(instance.core)
                .withName(instance.uuid.toLowerCase());
        DockerResponse<CreateContainerResponse> response = Nodes.get(instance.node).createContainerRemote(ccc);
        if(response.ok()){
            return response.response.createContainerResult.Id;
        }else{
            throw new Exception(response.message.message);
        }
    }
    public static void removeInstance(String id){
        Instance i=Instance.get(id);
        i.remove();
    }
}
