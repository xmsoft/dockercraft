package cn.tohsaka.factory.dockercraft.Engines;

import cn.tohsaka.factory.dockercraft.Handlers.EventBasedWebSocketHandler;
import cn.tohsaka.factory.dockercraft.Handlers.defaultHttpRequestHandler;
import cn.tohsaka.factory.dockercraft.Libraries.ConfigureProvider;
import org.webbitserver.WebServer;
import org.webbitserver.WebServers;

/**
 * Created by Xmsoft on 2017/8/3.
 */
public class webbitWrapper {
    public static WebServer webServer;
    public static EventBasedWebSocketHandler wse;
    public static void create() {
        webServer= WebServers.createWebServer(Integer.valueOf(ConfigureProvider.get("listening")));
        wse=new EventBasedWebSocketHandler();
        new defaultHttpRequestHandler();
        webServer.start();
    }
}
