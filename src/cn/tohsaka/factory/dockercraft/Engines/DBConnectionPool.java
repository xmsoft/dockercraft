package cn.tohsaka.factory.dockercraft.Engines;

import java.sql.Connection;
import java.util.Vector;

/**
 * Created by Xmsoft on 2016/7/31.
 */
public class DBConnectionPool {
    private static Vector<Connection> pool;
    private static int poolSize;
    private static String url;
    private static String username;
    private static String password;
    private static Thread pt;
    public static String driverClassName = "com.mysql.jdbc.Driver";//com.mysql.cj.jdbc.Driver

    public static void init(String u, String un, String pwd){
        url=u;username=un;password=pwd;
    }
    public static synchronized Connection getConnection() {
        try {
            return java.sql.DriverManager.getConnection(url, username, password);
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }
}