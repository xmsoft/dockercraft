package cn.tohsaka.factory.dockercraft.Engines;

import cn.tohsaka.factory.dockerclient.AsyncWriterEvent;
import cn.tohsaka.factory.dockerclient.DockerClient;
import cn.tohsaka.factory.dockerclient.beans.Container;
import cn.tohsaka.factory.dockerclient.beans.DockerResponse;
import cn.tohsaka.factory.dockercraft.Libraries.Logger;
import cn.tohsaka.factory.dockercraft.Structs.Users;
import com.google.gson.Gson;

import java.util.List;

/**
 * Created by Xmsoft on 2017/8/3.
 */
public class dockerWrapper {
    protected static Logger logger=new Logger(dockerWrapper.class);
    public static void create() {
        Nodes.getProvider().forEach((k,docker)->{
            System.out.println(k+" beging to pull required images");
            docker.pullImageRemote("q1051278389/dcterminal:latest", null, "https://registry.docker-cn.com", new AsyncWriterEvent() {
                @Override
                public void onWriteLine(String s) {
                    System.out.println(s);
                }

                @Override
                public void onEnd() {
                    System.out.println("pulling complate.");
                }
            });
            docker.jarlist=docker.getJarList();
        });
    }
}
