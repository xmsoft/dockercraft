package cn.tohsaka.factory.eoh.interfaces

import cn.tohsaka.factory.dockercraft.Engines.DBConnectionPool;
import cn.tohsaka.factory.dockercraft.Interfaces.DInterface
import cn.tohsaka.factory.dockercraft.Utils
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


/**
 * Created by Xmsoft on 2017/5/27.
 */
class DBRequester<T>(var clazz:Class<T>) {
    fun get(sql: String):T?{
        var rs=DBConnectionPool.getConnection()!!.prepareStatement(sql).executeQuery();
        val cons = clazz.newInstance() as DInterface;
        if(rs.next()) {
            return cons.parse(rs) as T;
        }
        return null;
    }
    fun getAll(sql: String):List<T>{
        var list=ArrayList<T>();
        var rs=DBConnectionPool.getConnection()!!.prepareStatement(sql).executeQuery();
        while (rs.next()){
            var cons = clazz.newInstance() as DInterface;
            list.add(cons.parse(rs) as T);
        }
        return list;
    }
}
fun DB(t:Any): DBRequester<Any> {
    return DBRequester(t as Class<Any>);
}