package cn.tohsaka.factory.dockercraft.Interfaces;

import cn.tohsaka.factory.dockercraft.Utils;
import cn.tohsaka.factory.dockercraft.utils.session;
import cn.tohsaka.factory.dockercraft.utils.sessions;
import org.webbitserver.WebSocketConnection;

/**
 * Created by Xmsoft on 2017/8/20.
 */
public class EventBasedWebSocketConnection {
    public WebSocketConnection client;
    public EventBasedWebSocketConnection(WebSocketConnection c){
        client=c;
    }
    public EventBasedWebSocketConnection emit(String event,Object data){
        WebSocketEventBean web=new WebSocketEventBean();
        web.e=event;
        web.data=data;
        client.send(Utils.toJson(web));
        return this;
    }
    public session session(){
        return sessions.get(client.httpRequest().cookieValue("sessid"));
    }
    public EventBasedWebSocketConnection  emit(String event,String data){
        WebSocketEventBean web=new WebSocketEventBean();
        web.e=event;
        web.data=data;
        client.send(Utils.toJson(web));
        return this;
    }

}
