package cn.tohsaka.factory.dockercraft;

import javax.rmi.CORBA.Util;
import java.util.HashMap;
import java.util.Map;

public class kvjsonBuilder{
    private Map<String,Object> map=new HashMap<>();
    public kvjsonBuilder put(String k,Object v){
        map.put(k,v);
        return this;
    }
    public String getJson(){
        return Utils.toJson(map);
    }
}
