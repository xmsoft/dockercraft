package cn.tohsaka.factory.dockercraft.Handlers;

import cn.tohsaka.factory.dockerclient.beans.sampleFilter;
import cn.tohsaka.factory.dockercraft.Interfaces.EventBasedWebSocketConnection;
import cn.tohsaka.factory.dockercraft.Libraries.EventHandler;
import cn.tohsaka.factory.dockercraft.Structs.Instance;
import cn.tohsaka.factory.dockercraft.Utils;

public class InstanceWsHandler extends EventHandler {
    @Override
    public void onEvent(String e, Object data, EventBasedWebSocketConnection wsc) {
        if(e.substring(0,4).equalsIgnoreCase("inst") && data.toString().trim().length()>0){
            Instance inst= Instance.get(data.toString(), Utils.toUid(wsc.session().get("uid")));
            if(inst==null){
                wsc.emit(e,"null");
                return;
            }
            switch (e.substring(5)){
                case "start":
                    if(inst.getDocker().listContainersRemote(sampleFilter.createByName(inst.uuid)).response.size()<1){
                        inst.createContainer();
                    }
                    inst.getDocker().startContainerRemote(inst.uuid);
                    break;
                case  "stop":
                    inst.stopInstance();
                    break;
                case "restart":
                    inst.restartInstance();
                    break;
            }
        }
    }
}
