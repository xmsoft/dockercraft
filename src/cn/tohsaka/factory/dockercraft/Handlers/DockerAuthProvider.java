package cn.tohsaka.factory.dockercraft.Handlers;

import cn.tohsaka.factory.dockercraft.Utils;
import org.webbitserver.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Xmsoft on 2017/8/9.
 */
public class DockerAuthProvider implements HttpHandler {
    public static List<String> keys=new ArrayList<>();
    @Override
    public void handleHttpRequest(HttpRequest httpRequest, HttpResponse httpResponse, HttpControl httpControl) throws Exception {
        httpResponse.status(200).end();
        if(keys.contains(httpRequest.header("authorization"))){
            keys.remove(httpRequest.header("authorization"));
            httpResponse.status(200).end();
        }else{
            httpResponse.status(403).end();
        }
    }
    public static String newKey(){
        String key=Utils.getRandomString(16);
        keys.add(key);
        return key;
    }
}
