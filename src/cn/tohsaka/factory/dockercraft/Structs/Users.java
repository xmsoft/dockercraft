package cn.tohsaka.factory.dockercraft.Structs;

import cn.tohsaka.factory.dockercraft.Interfaces.DInterface;
import cn.tohsaka.factory.dockercraft.Utils;
import cn.tohsaka.factory.eoh.interfaces.DBRequester;

/**
 * Created by Xmsoft on 2017/8/7.
 */
public class Users extends DInterface {
    @Override
    public String getTable(){
        return "dc_users";
    }
    public volatile int uid;
    public String un;
    public String pwd;
    public String salt;
    public static Users createUser(String r_un, String r_pwd){
        Users user=new Users();
        user.un=r_un;
        user.salt= Utils.getRandomString(8);
        user.pwd=Utils.hamcsha1(r_pwd,user.salt);
        user.uid = user.Insert(true);
        return user;
    }
    public static Users verifyUser(String un,String pwd){
        Users user=new DBRequester<Users>(Users.class).get("SELECT * FROM dc_users WHERE `un`='"+un+"'");
        if(user!=null && user.pwd.equalsIgnoreCase(Utils.hamcsha1(pwd,user.salt))){
            return user;
        }
        return null;
    }
    public static Boolean existsUser(String un){
        Users user=new DBRequester<Users>(Users.class).get("SELECT * FROM dc_users WHERE `un`='"+un+"'");
        if(user!=null){
            return true;
        }
        return false;
    }
}
