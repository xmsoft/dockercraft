package cn.tohsaka.factory.dockercraft.Structs;

import cn.tohsaka.factory.dockerclient.DockerClient;
import cn.tohsaka.factory.dockerclient.beans.DockerResponse;
import cn.tohsaka.factory.dockerclient.beans.details.create.CreateContainerCmd;
import cn.tohsaka.factory.dockerclient.beans.details.create.PortBinding;
import cn.tohsaka.factory.dockerclient.beans.details.create.RestartPolicy;
import cn.tohsaka.factory.dockercraft.Engines.Logcat;
import cn.tohsaka.factory.dockercraft.Engines.Nodes;
import cn.tohsaka.factory.dockercraft.Interfaces.DInterface;
import cn.tohsaka.factory.dockercraft.Utils;
import cn.tohsaka.factory.eoh.interfaces.DBRequester;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Xmsoft on 2017/8/7.
 */
public class Instance extends DInterface{
    @Override
    public String getTable(){
        return "dc_instances";
    }
    public volatile String uuid;
    public int uid;
    public int mem;
    public String core;
    public String node;
    public Long createTime;
    public String startOptions;
    public String label;
    public int port;
    public static Instance get(String uuid){
        Instance inst=null;
        try {
            inst = new DBRequester<Instance>(Instance.class).get(String.format("SELECT * FROM dc_instances WHERE uuid=%s", uuid));
        }catch (Exception e){
            Logcat.error(e);
        }
        return inst;
    }
    public static Instance get(String uuid,String uid){
        Instance inst=null;
        try {
            inst = new DBRequester<Instance>(Instance.class).get(String.format("SELECT * FROM dc_instances WHERE uuid=%s AND uid=%s", uuid,uid));
        }catch (Exception e){
            Logcat.error(e);
        }
        return inst;
    }
    public static Instance get(String uuid,int uid){
        Instance inst=null;
        try {
            System.out.println(String.format("SELECT * FROM dc_instances WHERE uuid=%s AND uid=%s", uuid,uid));
            inst = new DBRequester<Instance>(Instance.class).get(String.format("SELECT * FROM dc_instances WHERE uuid='%s' AND uid=%s", uuid,uid));
        }catch (Exception e){
            Logcat.error(e);
        }
        return inst;
    }
    public DockerClient getDocker(){
        return Nodes.get(this.node);
    }
    public boolean remove(){
        return getDocker().removeContainerRemote(uuid).ok();
    }

    public boolean createContainer(){
        CreateContainerCmd ccc=new CreateContainerCmd();
        List<String> envs=new ArrayList<>();
        try {
            envs=new Gson().fromJson(startOptions,new TypeToken<List<String>>(){}.getType());
        }catch (Exception e){
            Logcat.error(e);
        }
        ccc.withName(uuid)
                .withMemory(Integer.toUnsignedLong(mem))
                .withImage("q1051278389/dcterminal:latest")
                .withRestartPolicy(RestartPolicy.always())
                .withEnvs(envs)
                .withPortBinding("25565/tcp", new PortBinding("0.0.0.0", String.valueOf(port)))
                .withDictionaryBind(String.format("/mnt/data/idata/%s",uuid),"/data","rw")
                .withDictionaryBind(String.format("/mnt/data/servers",uuid),"/server","ro");
        DockerResponse dr = this.getDocker().createContainerRemote(ccc);
        return dr.ok();
    }
    public String stopInstance(){
        DockerResponse res =getDocker().stopContainerCmd(this.uuid);
        if(res.ok()){
            return "";
        }
        return  res.message.message;
    }
    public String restartInstance(){
        stopInstance();
        DockerResponse res =getDocker().startContainerCmd(this.uuid);
        if(res.ok()){
            return "";
        }
        return  res.message.message;
    }
}
