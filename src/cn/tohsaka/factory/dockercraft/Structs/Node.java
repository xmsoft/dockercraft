package cn.tohsaka.factory.dockercraft.Structs;

import cn.tohsaka.factory.dockercraft.Engines.Nodes;
import cn.tohsaka.factory.dockercraft.Interfaces.DInterface;
import cn.tohsaka.factory.dockercraft.Utils;

/**
 * Created by Xmsoft on 2017/8/7.
 */
public class Node extends DInterface {
    @Override
    public String getTable(){
        return "dc_nodes";
    }
    public volatile String uuid;
    public String label;
    public String ip;
    public int core;
    public long mem;
    public int allowInstances;
    public String auth;
}
