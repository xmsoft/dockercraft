package cn.tohsaka.factory.dockercraft;

import cn.tohsaka.factory.dockercraft.Engines.*;
import cn.tohsaka.factory.dockercraft.Libraries.ConfigureProvider;
import cn.tohsaka.factory.dockercraft.Libraries.Logger;
import com.google.gson.Gson;

public class LaunchWrapper {
    static Logger logger=new Logger(LaunchWrapper.class);
    public static void main(String[] args){
        ConfigureProvider.load();
        DBConnectionPool.init(ConfigureProvider.get("dbconstr"),ConfigureProvider.get("dbconuser"),ConfigureProvider.get("dbconpwd"));
        Nodes.init();
        dockerWrapper.create();
        pluginRegistry.create();
        Cardinal.init();
        webbitWrapper.create();
        //logger.printCopyright();
    }
}
